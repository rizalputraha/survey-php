<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends Mobile_Api_Controller
{
    protected $exception=['login'];
    public function __construct(){
        parent::__construct();
    }

    public function post_login()
    {
        $inp = $this->input->all();
        
        $token = $this->m_auth->login($inp['username'], $inp['password']);
        $message = "error login";

        if($token==="failed") {
            $message = "username atau password salah";
            return ['error'=>200, 'message'=>$message];
        } else {
            return ['error'=>0, 'data'=>['token'=> $token, 'user_data'=>$this->m_auth->user_data]];
        }
    }

    public function post_logout_user()
    {
        $res = $this->mobile_model->logout($this->user_data);
        return $res;
    }


}


?>