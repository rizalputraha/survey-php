<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service extends Mobile_Api_Controller {
    public function __construct()
	{
		parent::__construct();
		$this->load->model('m_service');
    }
    
    public function get_semua_wilayah(){
        $resp = $this->m_service->get_semua_wilayah();
        return $resp;
    }

    public function get_semua_survey()
    {
        $resp = $this->m_service->get_semua_survey();
        return $resp;
    }

    public function get_total_data(){
        $total = $this->m_service->get_total_data();
        return $total;
    }

    public function post_tambah_survey(){
        $inp = $this->input->all();
        $tambah_data = $this->m_service->post_tambah_survey($inp);
        return $tambah_data;
    }

    public function post_update_survey(){
        $inp = $this->input->all();
        $update = $this->m_service->post_update_survey($inp);
        return $update;
    }

    public function post_search_survey(){
        $inp = $this->input->all();
        $search_data = $this->m_service->post_search_survey($inp);
        return $search_data;
    }

    // Tahap 1.2
    public function post_update_survey2(){
        $inp = $this->input->all();
        $update = $this->m_service->post_update_survey2($inp);
        return $update;
    }

    // Tahap 2
    public function post_semua_tiang(){
        $inp = $this->input->all();
        $get_tiang = $this->m_service->post_semua_tiang($inp);
        return $get_tiang;
    }

    public function post_tambah_tiang(){
        $inp = $this->input->all();
        $add_tiang = $this->m_service->post_tambah_tiang($inp);
        return $add_tiang;
    }

    public function post_update_tiang(){
        $inp = $this->input->all();
        $update = $this->m_service->post_update_tiang($inp);
        return $update;
    }
}