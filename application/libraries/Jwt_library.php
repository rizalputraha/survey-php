<?php

/**
 *
 */
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha512;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;

class Jwt_library
{

  protected $signer;
  protected $secret='';
  public $salt='';
  protected $id='';
  protected $issuer='';
  protected $audience='';
  public $data=[];
  protected $expiration=172800;//48 Jam

  function __construct()
  {
    $this->signer=new Sha512();
  }

  public function create_token($data=array())
  {
    $this->data=$data;
    return (new Builder())->setIssuer($this->issuer)
                                        ->setAudience($this->audience)
                                        ->setId($this->id,true)
                                        ->setIssuedAt(time())
                                        ->setExpiration(time()+$this->expiration)
                                        ->set('data',json_encode($this->data))
                                        ->sign($this->signer, $this->secret.'.'.$this->salt)
                                        ->getToken()->__toString();
  }

  public function create_long_live_token($data=array())
  {
    $this->data=$data;
    return (new Builder())->setIssuer($this->issuer)
                                        ->setAudience($this->audience)
                                        ->setId($this->id,true)
                                        ->setIssuedAt(time())
                                        ->setExpiration((time()+31536000))
                                        ->set('data',json_encode($this->data))
                                        ->sign($this->signer, $this->secret.'.'.$this->salt)
                                        ->getToken()->__toString();
  }

  public function verify_token($token)
  {
    $atoken=(new Parser())->parse((string)$token);
    $data=new ValidationData();
    $data->setIssuer($this->issuer);
    $data->setAudience($this->audience);
    $data->setId($this->id);
    $verifier=$atoken->verify($this->signer, $this->secret.'.'.$this->salt);
    $valid=$atoken->validate($data);
    // var_dump($valid);
    // var_dump($verifier);
    return  $verifier && $valid;//&& (?$this->get_data($atoken):FALSE);
  }

  public function get_data($token)
  {
    return json_decode($token->getClaim('data'));
  }

  public function data($token)
  {
    try {
      $atoken=(new Parser())->parse((string)$token);
      return ['error'=>0, 'data'=>json_decode($atoken->getClaim('data'),TRUE)];
    } catch (Exception $e) {
      return ['error'=>200, 'message'=>'Token Invalid!'];
    }
  }

}
