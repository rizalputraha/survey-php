<?php

require_once (APPPATH.'/libraries/Jwt_library.php');
class Mobile_auth extends Jwt_library
{

  protected $secret='skhgdfiwejksbkcnpwejrpe294i-2elqwle;aslmas,c?>,zx./lkaswj';
  protected $issuer='http://localhost/Survey';
  protected $audience='http://localhost/Survey';

  function __construct()
  {
    parent::__construct();
  }

  public function do_login($data)
  {
    return $this->create_token($data);
  }

  public function do_login_v2($data)
  {
    return $this->create_long_live_token($data);
  }

}
