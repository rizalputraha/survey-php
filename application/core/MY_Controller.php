<?php

function utf8_encode_recursive($array)
{
    if (!is_array($array)) {
        return $array;
    }

    $result = array();
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $result[$key] = utf8_encode_recursive($value);
        } elseif (is_string($value)) {
            $result[$key] = utf8_encode($value);
        } else {
            $result[$key] = $value;
        }
    }
    return $result;
}

class Base_Main_Controller extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    // if (empty($this->session->userdata('user_id'))) {
    //   redirect('/');
    // }
  }
}

include_once "controllers/Simple_rest.php";
include_once "controllers/Angular.php";
include_once "controllers/Mobile.php";