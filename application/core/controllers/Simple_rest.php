<?php

class Simple_Rest_Controller extends Base_Main_Controller
{
	protected $content_type='application/json';
	protected $header=[];
	protected $disabled_log=true;
	protected $exception=[];
	protected $exclude_log=[];
	public $json_formated=TRUE;

	public function __construct()
	{
		parent::__construct();
		// if($this->load_session)$this->load->library('auth');
		if(!$this->disabled_log)
		{
			$this->load->library('data_logger');
		}
	}

	protected function authentication()
	{
		return true;
	}

	protected function get_username()
	{
		return $this->get_environment().'_'.$this->session->userdata('username');
	}

	protected function unset_sensitive_input($inp)
	{
		unset($inp['password']);
		unset($inp['pin']);
		unset($inp['mpin']);

		return $inp;
	}

	public function _remap($method)
	{
		$input=$this->input->all();
		$inp=$this->unset_sensitive_input($input);
		$msg=json_encode($inp);
		if((!in_array($method, $this->exception))&&($this->authentication()===FALSE))
		{
			if(!$this->disabled_log)$this->data_logger->write_log($this->get_username(), "Unprivileged [$method, $msg]", 'ERROR', $this->input->ip_address());
			$this->_unprivileged();
		}
		$func=(($this->input->method()==='put')||($this->input->method()==='patch'))?
			"update_$method":$this->input->method()."_".$method;

		if(method_exists($this, $func))
		{
			if((!in_array($method, $this->exclude_log))&&(!$this->disabled_log)) $this->data_logger->write_log($this->get_username(), "Request [$msg]", $method, $this->input->ip_address());
			try {
				$resp=$this->$func();
			} catch (Exception $e) {
				$resp['error']=101;
				$resp['message']=$e->getMessage();
			}
			$content=($this->content_type==='application/json')?json_encode(utf8_encode_recursive($resp)):$resp;
			$content_log=(!$content)?$resp:str_replace("\n",'',str_replace("\r",'',$content));
			if((!in_array($method, $this->exclude_log))&&(!$this->disabled_log)) $this->data_logger->write_log($this->get_username(), "Response [$content_log]", $method, $this->input->ip_address());
			$this->_response($content,200,$this->content_type,$this->header);
		}
		else
		{
			if(!$this->disabled_log)$this->data_logger->write_log($this->get_username(), "Method Not Found [$method, $msg]", 'ERROR', $this->input->ip_address());
			$this->_MethodNotFoundResponse();
		}
	}

	protected function _response($content, $status, $content_type)
	{
		if($this->config->item('enable_hooks')===TRUE){
	    require_once APPPATH.'hooks/Hooks.php';
	    $hook=new After_controller();
	    $hook->run([]);
    }
		$this->output->set_content_type($this->content_type)
						->set_status_header($status)
						->set_output($content)
						->_display();
		exit;
	}

	protected function _unprivileged()
	{
		$content=json_encode([
				'error'=>401,
				'message'=>'Unprivileged'
			]);
		$this->_response($content, 200, $this->content_type);
	}

	protected function _MethodNotFoundResponse()
	{
		$content=json_encode([
				'error'=>404,
				'message'=>'Method Not Found'
			]);
		$this->_response($content,200,$this->content_type);
	}
}
