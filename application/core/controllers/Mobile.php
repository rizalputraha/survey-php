<?php
/**
 *
 */
class Mobile_Api_Controller extends Simple_Rest_Controller
{
    protected $user_data;
    protected $load_session=false;
    protected $data_input;
    protected $m_model='m_auth';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->m_model, 'm_auth');
    }

    protected function authentication()
    {
        if ($this->inp_token()==FALSE) {
            return false;
        }
        $resp=$this->m_auth->verify_token($this->inp_token());
        if ($resp===false) {
            return false;
        }
        $resp=$this->m_auth->get_data($this->inp_token());
        if ($resp['error']!=0) {
            return false;
        }
        $this->user_data=$resp['data'];
        return true;
    }

    protected function get_username()
    {
        return $this->get_environment().'_'.(isset($this->user_data['username'])?$this->user_data['username']:'');
    }

    protected function unset_sensitive_input($inp)
    {
        if (is_string($inp)) {
            $inp=json_decode($inp, true);
        }
        try {
            if (isset($inp['data'])) {
                if (isset($inp['data']['password'])) {
                    unset($inp['data']['password']);
                }
                if (isset($inp['data']['pin'])) {
                    unset($inp['data']['pin']);
                }
                if (isset($inp['data']['mpin'])) {
                    unset($inp['data']['mpin']);
                }
            }
        } catch (Exception $e) {
        }
        return $inp;
    }

    private function inp_token()
    {
        $inp=$this->input->all();
        $inp=is_string($inp)?json_decode($inp, true):$inp;
        // $this->data_input=is_string($inp['data'])?json_decode($inp['data'],TRUE):$inp['data'];
        $this->data_input=isset($inp['data'])?(is_string($inp['data'])?json_decode($inp['data'], true):$inp['data']):[];
        // $token=isset($_SERVER['HTTP_AUTH_TOKEN'])?$_SERVER['HTTP_AUTH_TOKEN']:$inp['token'];
        if (isset($_SERVER['HTTP_AUTH_TOKEN'])) {
            return $_SERVER['HTTP_AUTH_TOKEN'];
        } else {
            if (isset($inp['token'])) {
                return $inp['token'];
            }else{
                return FALSE;
            }
        }
        // return $token;//$this->mobile_model->verify_token($token);
    }

    protected function _unprivileged()
    {
        $content=json_encode([
                'error'=>401,
                'message'=>'Unprivileged/Session Expired, Please Login Again!'
            ]);
        $this->_response($content, 200, $this->content_type);
    }
}
