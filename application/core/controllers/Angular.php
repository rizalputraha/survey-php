<?php


class Angular_View_Controller extends CI_Controller
{
	protected $load_session=false;
	protected $view_folder;

	public function __construct()
	{
		parent::__construct();
		if(!$this->authentication())$this->error_auth();
		// $this->load->library('auth');
		// $this->output->set_compress_output(TRUE);
		$this->before_process();
	}

	protected function before_process()
	{

	}

	protected function error_auth()
	{
		echo "Auth Error!";
		exit;
	}

	protected function authentication()
	{
		return true;
	}

	public function index()
	{
		$this->load->view($this->view_folder.'/layout');
	}

	public function templates($name)
	{
		$data=[];
		$fp=str_replace('.', '/', $name);
		$func='templates_'.str_replace('.', '_', $name);
		if(method_exists($this,$func))
		{
			$data=$this->{$func}();
			if(isset($data['cache'])&&($data['cache']))
			{
				header('Cache-Control: public, max-age='.(60*60*2));
				header_remove('Pragma');
			}
			else
			{
				header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
				header('Pragma: no-cache');
			}
		}
		else
		{
			header('Cache-Control: public, max-age='.(60*60*24*3));
		}
		header_remove('Pragma');
		$this->load->view($this->view_folder.'/templates/'.$fp,$data);
	}

	public function js($name)
	{
		$data=[];
		$fp=str_replace('.', '/', $name);
		$func='js_'.str_replace('.', '_', $name);
		if(method_exists($this, $func))
		{
			$data=$this->{$func}();
			if(isset($data['cache'])&&($data['cache']))
			{
				header('Cache-Control: public, max-age='.(60*60*2));
				header_remove('Pragma');
			}
			else
			{
				header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
				header('Pragma: no-cache');
			}
		}
		else
		{
			header('Cache-Control: public, max-age='.(60*60*24*5));
			header_remove('Pragma');
		}
		header('Content-Type: application/javascript; charset=utf-8');
		$this->load->view($this->view_folder.'/js/'.$fp,$data);
	}
}
