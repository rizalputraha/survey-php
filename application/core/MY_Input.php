<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Input extends CI_Input
{
	public function http_origin()
	{
		return (isset($_SERVER['HTTP_ORIGIN']))?$_SERVER['HTTP_ORIGIN']:'*';
	}

	public function method($upper=false)
	{
		return ($upper)
			? strtoupper($this->server('REQUEST_METHOD'))
			: strtolower($this->server('REQUEST_METHOD'));
	}

	public function ip_address()
	{
		foreach (array('HTTP_CF_CONNECTING_IP', 'HTTP_X_REAL_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_CLIENT_IP', 'HTTP_X_CLIENT_IP', 'HTTP_X_CLUSTER_CLIENT_IP') as $header)
		{
			if (($spoof = $this->server($header)) !== NULL)
			{
				// Some proxies typically list the whole chain of IP
				// addresses through which the client has reached us.
				// e.g. client_ip, proxy_ip1, proxy_ip2, etc.
				sscanf($spoof, '%[^,]', $spoof);

				if ( ! $this->valid_ip($spoof))
				{
					$spoof = NULL;
				}
				else
				{
					break;
				}
			}
		}

		return isset($spoof)?$spoof:parent::ip_address();
		// return isset($_SERVER['']);//parent::ip_address();
	}

	public function jwt_token_verified()
	{
		$token=$this->get_request_header('Auth-Token');
		$jwt =& load_class('jwt_library');
		return $jwt->verify_token($token);
	}

	public function post_all()
	{
		$r=[];
		foreach ($_POST as $key => $value)
		{
			$r[$key]=$this->post($key);
		}
		return $r;
	}

	public function get_all()
	{
		$r=[];
		foreach ($_GET as $key => $value)
		{
			$r[$key]=$this->get($key);
		}
		return $r;
	}

	public function all()
	{
		$r=$this->post_all();
		$stream=$this->__get('raw_input_stream');
		$stream=isset($stream)?json_decode($stream,TRUE):'';
		$r=array_merge($r, $this->get_all());

		return array_merge($r,(array)$stream);
	}
}
