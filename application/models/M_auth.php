<?php

class M_auth extends CI_Model
{
    public $user_data;
    public $salt = 'SomeStringForSalt';

    public function __construct()
    {
        parent::__construct();
        $this->load->library('mobile_auth');
    }

    public function login($username, $password)
    {
        $passw = md5($this->salt.$password);
        $qry = $this->db->select('id, username, level')
                ->where('username', $username)
                ->where('password', $passw)
                ->limit(1)
                ->get('surveyor');
        $user=$qry->row();

        return $qry->num_rows()>0?$this->create_token($user, $user->level.$passw.$user->id):"failed";
    }

    public function verify_token($token)
    {
        $data=$this->mobile_auth->data($token);
        if ($data['error']!=0) {
            return $data;
        }
        $qry=$this->db->select('password, id, level')
                    ->where('username', $data['data']['username'])
                    ->where('level', $data['data']['level'])
                    ->get('surveyor');

        $user=$qry->num_rows()>0?$qry->row():false;
        if ($user===false) {
            return false;
        }
        $this->mobile_auth->salt=$user->level.$user->password.$user->id;
        return $this->mobile_auth->verify_token($token);
    }

    protected function create_token($user, $salt)
    {
        $this->user_data=$user;
        $this->mobile_auth->salt=$salt;
        return $this->mobile_auth->do_login_v2($user);
    }

    public function get_data($token)
    {
        return $this->mobile_auth->data($token);
    }
    
}
