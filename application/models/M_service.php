<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_service extends CI_Model {

    public $variable;
    private $table = 'Survey';
    private $tiang = 'Tiang';

	public function __construct()
	{
		parent::__construct();
        $this->load->library('session');
    }

    private function upload($dir,$name ='userfile',$filename=null){
		$config['upload_path']     = $dir;
        $config['allowed_types']   = 'gif|jpg|png|jpeg';
        $config['max_size']        = 50000000;
        $config['encrypt_name'] 	 = FALSE;
        $config['file_name'] 		   = $filename;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload($name))
        {
        		$data['auth'] 	= false;
                $data['msg'] 	= $this->upload->display_errors();
                return $data;
        }
        else
        {
        		$data['auth']	= true;
        		$data['msg']	= $this->upload->data();
        		return $data;
        }
	}

	private function isImage($file){
		if ((($_FILES[$file]['type'] == 'image/gif') || ($_FILES[$file]['type'] == 'image/jpeg') || ($_FILES[$file]['type'] == 'image/png'))){
			return true;
		}
		else {
			return false;
		}
    }

    function selectNoUrut($wilayah)
	{
		$this->db->select_max('NoUrut');
        $this->db->where('Wilayah', $wilayah);
        $res1 = $this->db->get('Survey')->result();
		return $res1[0];
    }

    function selectNoTiang($idpel)
	{
		$this->db->select_max('NoTiang');
        $this->db->where('IDPel', $idpel);
        $res1 = $this->db->get('Tiang')->result();
		return $res1[0];
	}
    
    public function get_semua_survey(){
        $res = $this->db->get($this->table);
        if($res->num_rows() < 0){
            return ['error' => 500, 'message' => 'Data Kosong'];    
        }else{
            return ['error' => 0, 'data' => $res->result()];
        }
    }

	public function get_semua_wilayah(){
        $this->db->select('Wilayah');
		$this->db->group_by('Wilayah');
        $res = $this->db->get($this->table);
        
        $wilayah = array();
        foreach ($res->result() as $row) {
            $wilayah[] = $row->Wilayah;
        }
        if($res->num_rows() < 0){
            return ['error' => 500, 'message' => 'Data Kosong'];    
        }else{
            return ['error' => 0, 'data' => $wilayah];
        }
    }

    public function get_total_data(){
        $res = $this->db->get($this->table)->num_rows();
        if($res < 0){
            return ['error' => 500, 'message' => 'Data Kosong'];    
        }else{
            return ['error' => 0, 'data' => $res];
        }
    }

    public function post_search_survey($inp){
        if(!isset($inp['IDPel'])){
            if (!isset($inp['Wilayah'])) {
                $res = $this->db->get($this->table)->result();
                return ['error'=> 0, 'data' => $res];
            }
            $where = array(
                'Wilayah' => $inp['Wilayah'],
            );
            $this->db->like($where);
            $res = $this->db->get($this->table)->result();
            return ['error'=> 0, 'data' => $res];
        }elseif($inp['Wilayah'] != null && $inp['IDPel'] != null){
            $where = array(
                'Wilayah' => $inp['Wilayah'],
                'IDPel' => $inp['IDPel'],
            );
            $this->db->like($where);
            $res = $this->db->get($this->table)->result();
            return ['error'=> 0, 'data' => $res];
        }else{
            return ['error'=> 1, 'message' => 'Failed To Get Data'];
        }
    }

    public function post_tambah_survey($inp){
        
        $no = $this->selectNoUrut($inp['Wilayah']);
        $filename = $inp['IDPel'].".jpg";

        if (!empty($_FILES['KondisiBox']['name'])) {
            $filename = $inp['IDPel'].".jpg";
            $fullpath = 'assets/'. $filename;

            if (file_exists($fullpath)) {
                unlink('assets/'.$inp['IDPel'].".jpg");
            }

            $upload 	= $this->upload('./assets/','KondisiBox',$inp['IDPel']);


            if($upload['auth']	== false){
                echo $this->toJsonData(404,$upload['msg']);
                return;
            }


            $fotoname 	= $upload['msg']['file_name'];
            if(!empty($fotoname)){
                delete_files(base_url().'assets/'.$inp['IDPel'].".jpg");
            }
        }
        $data = array(
            'NoUrut'        => $no->NoUrut+1,
            'IDPel'         => $inp['IDPel'],
            'KWhMeter'      => $inp['Kwh'],
            'MCB'           => $inp['Mcb'],
            'Grounding'     => $inp['Ground'],
            'KondisiBox'    => $filename,
            'StandKWH1'     => $inp['Stand'],
            'PembatasDaya'  => $inp['Batas'],
            'Switch'        => $inp['Switch'],
            'Tanggal1'      => $inp['Tanggal'],
            'Latitude'      => $inp['Lat'],
            'Ampere'        => $inp['Amp'],
            'Nama'          => $inp['Nama'],
            'Alamat'        => $inp['Alamat'],
            'Longitude'     => $inp['Long'],
            'Keterangan'    => $inp['Ket'],
            'JumlahLampu'   => $inp['Jumlah'],
            'JumlahMCB'     => $inp['JumlahMCB'],
            'Kontaktor'     => $inp['Kontaktor'],
            'CosPhi'	    => $inp['Cospi'],
            'VoltAmpere'    => $inp['VoltAmpere'],
            'Daya'          => $inp['Daya'],
            'Watt'          => $inp['Watt'],
            'Provinsi'      => $inp['Kd_Provinsi'],
            'KabupatenKota' => $inp['Kd_Kabupaten'],
            'Kecamatan'     => $inp['Kd_Kecamatan'],
            'Nama'          => $inp['Nama'],
            'Alamat'        => $inp['Alamat'],
            'Wilayah'       => $inp['Wilayah'],
            'KabelInner'    => $inp['KabelInner'],
            'KabelOuter'    => $inp['KabelOuter'],
            'Desa'          => $inp['Desa'],
            'IdSurveyor'    => $inp['IdSurveyor']
        );
        $res = $this->db->insert($this->table,$data);
        if(!$res){
            return ['error' => 0, 'message' => 'Gagal Input Data'];
        }
        else{
            return ['error' => 0, 'message' => 'Sukses Input Data'];
        }
    }

    public function post_update_survey($inp)
    {
        $no = $this->selectNoUrut($inp['Wilayah']);
        $filename = $inp['IDPel'].".jpg";

        if (!empty($_FILES['KondisiBox']['name'])) {
            $filename = $inp['IDPel'].".jpg";
            $fullpath = 'assets/'. $filename;

            if (file_exists($fullpath)) {
                unlink('assets/'.$inp['IDPel'].".jpg");
            }

            $upload 	= $this->upload('./assets/','KondisiBox',$inp['IDPel']);


            if($upload['auth']	== false){
                echo $this->toJsonData(404,$upload['msg']);
                return;
            }


            $fotoname 	= $upload['msg']['file_name'];
            if(!empty($fotoname)){
                delete_files(base_url().'assets/'.$inp['IDPel'].".jpg");
            }
        }
        $data = array(
            'NoUrut'        => $inp['NoUrut'],
            'IDPel'         => $inp['IDPel'],
            'KWhMeter'      => $inp['Kwh'],
            'MCB'           => $inp['Mcb'],
            'Grounding'     => $inp['Ground'],
            'KondisiBox'    => $filename,
            'StandKWH1'     => $inp['Stand'],
            'PembatasDaya'  => $inp['Batas'],
            'Switch'        => $inp['Switch'],
            'Tanggal1'      => $inp['Tanggal'],
            'Latitude'      => $inp['Lat'],
            'Ampere'        => $inp['Amp'],
            'Nama'          => $inp['Nama'],
            'Alamat'        => $inp['Alamat'],
            'Longitude'     => $inp['Long'],
            'Keterangan'    => $inp['Ket'],
            'JumlahLampu'   => $inp['Jumlah'],
            'JumlahMCB'     => $inp['JumlahMCB'],
            'Kontaktor'     => $inp['Kontaktor'],
            'CosPhi'	    => $inp['Cospi'],
            'VoltAmpere'    => $inp['VoltAmpere'],
            'Daya'          => $inp['Daya'],
            'Watt'          => $inp['Watt'],
            'Provinsi'      => $inp['Kd_Provinsi'],
            'KabupatenKota' => $inp['Kd_Kabupaten'],
            'Kecamatan'     => $inp['Kd_Kecamatan'],
            'Nama'          => $inp['Nama'],
            'Alamat'        => $inp['Alamat'],
            'Wilayah'       => $inp['Wilayah'],
            'KabelInner'    => $inp['KabelInner'],
            'KabelOuter'    => $inp['KabelOuter'],
            'Desa'          => $inp['Desa'],
            'IdSurveyor'    => $inp['IdSurveyor']
        );
        $this->db->where(['IDPel' => $inp['IDPel']]);
        $res = $this->db->update($this->table,$data);
        if(!$res){
            return ['error' => 0, 'message' => 'Gagal Update Data'];
        }
        else{
            return ['error' => 0, 'message' => 'Sukses Update Data'];
        }
    }

    // Tahap 1.2
    function post_update_survey2($inp){
        if (!isset($inp['IDPel'])) {
			return ['error' => 0, 'message' => 'silahkan masukkan id pelanggan'];
        }
        $data = [
            'StandKWH2'    => $inp['StandKWH2'],
            'Tanggal2'     => $inp['Tanggal2']
        ];
        $this->db->where(['IDPel' => $inp['IDPel']]);
        $res = $this->db->update($this->table,$data);
        if(!$res){
            return ['error' => 0, 'message' => 'Gagal Simpan Data'];
        }
        else{
            return ['error' => 0, 'message' => 'Sukses Simpan Data'];
        }
    }

    // Tahap 2
    function post_semua_tiang($inp){
        if(!isset($inp['IDPel'])){
            return ['error'=> 1, 'message' => 'silahkan masukkan id pelanggan'];
        }else if(isset($inp['IDPel'])){
            $where = array(
                'IDPel' => $inp['IDPel'],
            );
            $this->db->where($where);
            
            $res = $this->db->get($this->table);
            if ($res->num_rows() < 0) {
                return ['error'=> 1, 'message' => 'Data Kosong'];
            }else{
                return ['error'=> 0, 'data' => $res->result()];
            }
        }else{
            return ['error'=> 1, 'message' => 'Failed To Get Data'];
        }
    }

    function post_tambah_tiang($inp){
        if (!isset($inp['IDPel'])) {
            return ['error'=> 1, 'message' => 'gagal tambah data masukkan id pelanggan'];
        }else{
            $noQuery = $this->selectNoTiang($inp['IDPel']);
            $no = $noQuery->NoTiang+1;
        
            if (!empty($_FILES['KondisiTiang']['name'])) {
                $filename = $inp['IDPel'].'-'.$no.".jpg";
                $fullpath = 'assets/tiang/'.$filename;

                if (file_exists($fullpath)) {
                    unlink('assets/tiang/'.$inp['IDPel'].'-'.$no.".jpg");
                }

                $upload 	= $this->upload('./assets/tiang/','KondisiTiang',$filename);

                $foto = [
                    'NoTiang' => $no,
                    'IDPel' => $inp['IDPel'],
                    'Nama' => $inp['Nama'],
                    'JenisTiang' => $inp['JenisTiang'],
                    'Lat' => $inp['Lat'],
                    'Lng' => $inp['Lng'],
                    'JumlahLampu' => $inp['JumlahLampu'],
                    'JenisLampu' => $inp['JenisLampu'],
                    'DayaLampu' => $inp['DayaLampu'],
                    'VALampu' => $inp['VALampu'],
                    'KondisiTiang' => $filename,
                    'KeteranganTiang' => $inp['KeteranganTiang'],
                    'JenisKabel' => $inp['JenisKabel'],
                ];

                $tiang = $this->db->insert('tiang',$foto);

                if($upload['auth']	== false){
                    return ['error' => 1,'message' => $upload['msg']];
                }

                $fotoname 	= $upload['msg']['file_name'];
                if(!empty($fotoname)){
                    // remFile(base_url().'assets/'.$id_pel.".jpg");
                    delete_files(base_url().'assets/tiang/'.$inp['IDPel'].'-'.$no.".jpg");
                }
                if ($tiang) {
                    return ['error' => 0, 'message' => 'Sukses Simpan Data'];
                }else{
                    return ['error' => 1, 'message' => 'Gagal Simpan Data'];
                }
            }else{
                $data = array(
                    'NoTiang' => $no,
                    'IDPel' => $inp['IDPel'],
                    'Nama' => $inp['Nama'],
                    'JenisTiang' => $inp['JenisTiang'],
                    'Lat' => $inp['Lat'],
                    'Lng' => $inp['Lng'],
                    'JumlahLampu' => $inp['JumlahLampu'],
                    'JenisLampu' => $inp['JenisLampu'],
                    'DayaLampu' => $inp['DayaLampu'],
                    'VALampu' => $inp['VALampu'],
                    'KeteranganTiang' => $inp['KeteranganTiang'],
                    'JenisKabel' => $inp['JenisKabel'],
                );
                
                $tiang = $this->db->insert('tiang',$data);
                
                $res = ($tiang ? ['error' => 0, 'message' => 'Sukses Simpan Data'] : ['error' => 1, 'message' => 'Gagal Simpan Data']);
                return $res;
            }
        }
    }

    function post_update_tiang($inp){
        if (!isset($inp['IDPel']) && !isset($inp['IDTiang'])) {
            return ['error'=> 1, 'message' => 'gagal tambah data masukkan id pelanggan'];
        }else{
            $data = $this->db->get_where('Tiang',['IDPel' => $inp['IDPel']]);
            $noQuery = $this->selectNoTiang($inp['IDPel']);
            $no = $noQuery->NoTiang+1;

            if ($data->num_rows() < 0) {
                return ['error' => 1, 'message' => 'data tidak ada'];
            }
        
            if (!empty($_FILES['KondisiTiang']['name'])) {
                $filename = $inp['IDPel'].'-'.$no.".jpg";
                $fullpath = 'assets/tiang/'.$filename;

                if (file_exists($fullpath)) {
                    unlink('assets/tiang/'.$inp['IDPel'].'-'.$no.".jpg");
                }

                $upload 	= $this->upload('./assets/tiang/','KondisiTiang',$filename);

                $foto = [
                    'NoTiang' => $no,
                    'IDPel' => $inp['IDPel'],
                    'Nama' => $inp['Nama'],
                    'JenisTiang' => $inp['JenisTiang'],
                    'Lat' => $inp['Lat'],
                    'Lng' => $inp['Lng'],
                    'JumlahLampu' => $inp['JumlahLampu'],
                    'JenisLampu' => $inp['JenisLampu'],
                    'DayaLampu' => $inp['DayaLampu'],
                    'VALampu' => $inp['VALampu'],
                    'KondisiTiang' => $filename,
                    'KeteranganTiang' => $inp['KeteranganTiang'],
                    'JenisKabel' => $inp['JenisKabel'],
                ];

                $this->db->where(['IDTiang' => $inp['IDTiang']]);
                $tiang = $this->db->update('tiang',$foto);

                if($upload['auth']	== false){
                    return ['error' => 1,'message' => $upload['msg']];
                }

                $fotoname 	= $upload['msg']['file_name'];
                if(!empty($fotoname)){
                    // remFile(base_url().'assets/'.$id_pel.".jpg");
                    delete_files(base_url().'assets/tiang/'.$inp['IDPel'].'-'.$no.".jpg");
                }
                if ($tiang) {
                    return ['error' => 0, 'message' => 'Sukses Update Data'];
                }else{
                    return ['error' => 1, 'message' => 'Gagal Update Data'];
                }
            }else{
                $data = array(
                    'NoTiang' => $no,
                    'IDPel' => $inp['IDPel'],
                    'Nama' => $inp['Nama'],
                    'JenisTiang' => $inp['JenisTiang'],
                    'Lat' => $inp['Lat'],
                    'Lng' => $inp['Lng'],
                    'JumlahLampu' => $inp['JumlahLampu'],
                    'JenisLampu' => $inp['JenisLampu'],
                    'DayaLampu' => $inp['DayaLampu'],
                    'VALampu' => $inp['VALampu'],
                    'KeteranganTiang' => $inp['KeteranganTiang'],
                    'JenisKabel' => $inp['JenisKabel'],
                );
                
                $this->db->where(['IDTiang' => $inp['IDTiang']]);
                $tiang = $this->db->update('tiang',$data);
                
                $res = ($tiang ? ['error' => 0, 'message' => 'Sukses Update Data'] : ['error' => 1, 'message' => 'Gagal Update Data']);
                return $res;
            }
        }
    }
}

/* End of file M_service.php */
/* Location: ./application/models/M_service.php */
